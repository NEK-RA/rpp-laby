import org.junit.Test;
import static org.junit.Assert.*;
public class TaskExTest {
    private CQueue tiq;
    private CQueue tuq;
    private int lim = 50;
    private Task testTask = new Task(0,"Москва");

    @Test
    public void runTest() throws InterruptedException{
        tiq = new CQueue(lim);
        tiq.add(testTask);
        tuq = new CQueue(lim);
        TaskExecutor te = new TaskExecutor(tiq,tuq);
        Thread tes = new Thread(te);

        tes.start();
        Thread.sleep(100);
        tes.interrupt();

        assertEquals(0,tiq.size());
        assertEquals(1,tuq.size());
        Task res = tuq.get();
        assertEquals("+10",res.weather);
    }
}
