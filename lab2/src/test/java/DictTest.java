import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Set;

public class DictTest {
    @Test
    public void testSize(){
        int dictSize=Dict.Weather.keySet().size();
        assertEquals(5,dictSize);
    }

    @Test
    public void testCities(){
        Set<String> keys=Dict.Weather.keySet();
        assertTrue(keys.contains("Москва"));
        assertTrue(keys.contains("Владимир"));
        assertTrue(keys.contains("Новосибирск"));
        assertTrue(keys.contains("Калининград"));
        assertTrue(keys.contains("Краснодар"));
    }

    @Test
    public void testWeathers(){
        assertEquals("+10",Dict.Weather.getWeather("Москва"));
        assertEquals("+15",Dict.Weather.getWeather("Владимир"));
        assertEquals("0",Dict.Weather.getWeather("Новосибирск"));
        assertEquals("+5",Dict.Weather.getWeather("Калининград"));
        assertEquals("+25",Dict.Weather.getWeather("Краснодар"));
    }
}
