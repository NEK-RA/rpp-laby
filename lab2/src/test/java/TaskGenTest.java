import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TaskGenTest {
    private CQueue tiq;
    private TaskGenerator tg;
    private int lim=50;

    @Test
    public void runTest(){
        tiq = new CQueue(lim);
        tg = new TaskGenerator(tiq);
        Thread tgs = new Thread(tg);
        tgs.start();

        try{
            Thread.sleep(100);
        }catch(InterruptedException e) {
            e.printStackTrace();
        }

        tgs.interrupt();
        assertTrue(tiq.size()<=lim);
        assertNotNull(tiq.get());
    }
}
