import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public enum Dict {
    Weather();

    private Map<String,String> weather = new HashMap<>();

    Dict(){
        this.weather.put("Москва","+10");
        this.weather.put("Владимир", "+15");
        this.weather.put("Новосибирск","0");
        this.weather.put("Краснодар","+25");
        this.weather.put("Калининград","+5");
    }

    public String getWeather(String city) {
        return this.weather.get(city);
    }

    public Set<String> keySet(){
        return weather.keySet();
    }
}
