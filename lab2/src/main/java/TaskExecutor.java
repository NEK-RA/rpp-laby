public class TaskExecutor implements Runnable {
    private CQueue genQ;
    private CQueue exQ;

    public TaskExecutor(CQueue inQ, CQueue outQ){
        this.genQ=inQ;
        this.exQ=outQ;
    }

    private void exct(){
        Task curTask = genQ.get();
        if(curTask!=null){
            curTask.weather=Dict.Weather.getWeather(curTask.city);
            exQ.add(curTask);
        }
    }

    @Override
    public void run(){
        try{
            while(!Thread.interrupted()){
                while(!genQ.isEmpty()){
                    exct();
                }
                Thread.sleep(100);
            }
        }catch (InterruptedException e){
            //e.printStackTrace();
        }
    }
}
