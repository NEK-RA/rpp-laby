import java.util.LinkedList;
import java.util.Queue;

public class CQueue {

    private Queue<Task> taskQueue = new LinkedList<>();
    private int maxSize;

    public CQueue(int maxSize) {
        this.maxSize = maxSize;
    }

    public boolean add(Task task) {
        if (size() < maxSize) {
            synchronized (this) {
                if (size() < maxSize) {
                    taskQueue.add(task);
                }
            }
            return true;
        }
        else {
            return false;
        }
    }

    public Task get() {
        if (!isEmpty()) {
            synchronized (this) {
                if (!isEmpty()) {
                    return this.taskQueue.poll();
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }

    }

    public boolean isEmpty(){
        return this.taskQueue.isEmpty();
    }

    public int size(){
        return this.taskQueue.size();
    }
}
