package ru.mirea.weather;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.mirea.weather.WeatherService;

public class WeatherService_Test {
    @Test
    public void testCities(){
        WeatherService ws = new WeatherService();
        assertEquals(5,ws.availableCities().size());
    }
    @Test
    public void testWeather(){
        WeatherService ws = new WeatherService();
        assertEquals("+10",ws.getWeather("Москва"));
    }
}
