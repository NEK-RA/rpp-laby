package ru.mirea.weather;
import org.junit.Test;
import static org.junit.Assert.*;
public class QueueTest {
    private CQueue testQ;
    private int lim=50;
    private Task testTask = new Task(0,"NoNameCity");

    @Test
    public void testSize(){
        testQ = new CQueue(lim);
        assertEquals(0,testQ.size());
        testQ.add(testTask);
        assertEquals(1,testQ.size());
        testQ.get();
        assertEquals(0,testQ.size());
        assertTrue(testQ.isEmpty());
    }

}
