package ru.mirea.weather;
import org.junit.Test;
import static org.junit.Assert.*;
public class LoggerTest {
    CQueue testQ;

    @Test
    public void testRun(){
        testQ=new CQueue(50);
        Task testTask = new Task(0,"Unreal Cold");
        testTask.weather="-273";
        testQ.add(testTask);
        testTask = new Task(1,"Another Planet");
        testTask.weather="+600";
        testQ.add(testTask);

        Logger logout = new Logger(testQ);
        Thread logWorker = new Thread(logout);

        logWorker.start();

        try{
            Thread.sleep(1000);
        }catch (InterruptedException e){
            //e.printStackTrace();
        }

        logWorker.interrupt();

        assertTrue(testQ.isEmpty());

    }
}
