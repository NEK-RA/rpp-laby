package ru.mirea.weather;

public class Logger implements Runnable {
    CQueue outQ;

    public Logger(CQueue outQ) {
        this.outQ = outQ;
    }

    @Override
    public void run() {
        while(!outQ.isEmpty()){
            this.show();
        }
    }

    private void show(){
        try {
            while (!Thread.interrupted()){
                Task task = outQ.get();
                if (task == null){
                    Thread.sleep(20);
                }else {
                    System.out.println("Инфа о задаче:");
                    System.out.println("ID: "+task.id);
                    System.out.println("Город: "+task.city);
                    System.out.println("Погода: "+task.weather);
                    System.out.println("Для даты: "+task.date);
                    System.out.println("\n\n");
                    Thread.sleep(10);
                }
            }
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }
}

