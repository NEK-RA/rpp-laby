package ru.mirea.weather;
public class TaskGenerator implements Runnable {
    private CQueue genQ;
    private int idCounter=0;
    private WeatherService ws = new WeatherService();

    public TaskGenerator(CQueue inQ){
        this.genQ=inQ;
    }

    private void addTask(String city){
        Task newTask = new Task(idCounter,city);
        genQ.add(newTask);
        idCounter++;
    }

    @Override
    public void run(){
        try{
            while(!Thread.interrupted()){
                for(String city : ws.availableCities()) {
                    addTask(city);
                    Thread.sleep(100);
                }
            }
        }catch (InterruptedException e){
            //e.printStackTrace();
        }
    }
}
