package ru.mirea.weather;
import ru.mirea.DataSourceImpl;
import java.util.Set;

public class WeatherService {
    private DataSourceImpl dataSource;

    public WeatherService() {
        this.dataSource = new DataSourceImpl();
    }

    public Set<String> availableCities(){
        Set<String> cities = this.dataSource.keySet();
        System.out.println(cities.size());
        return cities;
    }

    public String getWeather(String city){
        return this.dataSource.getWeather(city);
    }

    public static void main(String[] args){
        WeatherService ws = new WeatherService();
        CQueue inQ = new CQueue(50);
        CQueue outQ = new CQueue(50);

        TaskGenerator TGObj = new TaskGenerator(inQ);
        Thread generator = new Thread(TGObj);

        TaskExecutor TEObjF = new TaskExecutor(inQ, outQ);
        Thread executor1 = new Thread(TEObjF);
        Thread executor2 = new Thread(TEObjF);

        Logger LObj = new Logger(outQ);
        Thread loggerWorker = new Thread(LObj);

        generator.start();
        executor1.start();
        executor2.start();
        loggerWorker.start();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        generator.interrupt();
        executor1.interrupt();
        executor2.interrupt();
        loggerWorker.interrupt();
    }
}
