package ru.mirea;
import java.util.Set;

public interface DataSourceAPI {
    Set<String> keySet();
    String getWeather(String city);
}
