package ru.mirea;
import ru.mirea.DataSourceAPI;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;

public class DataSourceImpl implements DataSourceAPI{
    private Map<String,String> cities = new HashMap<String, String>();

    public DataSourceImpl(){
        cities.put("Москва","+10");
        cities.put("Владимир","+15");
        cities.put("Новосибирск","0");
        cities.put("Краснодар","+25");
        cities.put("Калининград","+5");
    }

    public Set<String> keySet(){
        return this.cities.keySet();
    }

    public String getWeather(String city){
        return this.cities.get(city);
    }
    
}
